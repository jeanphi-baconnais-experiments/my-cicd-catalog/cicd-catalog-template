# <Catalog template name> 
[![Latest Release](https://gitlab.com/<your project path>/-/badges/release.svg)](https://gitlab.com/<your project path>/-/releases) 


## ✨ Contributions

This project is opensource, under this [licence](./LICENCE).

Don't hesitate to create an issue to suggest anything or declare a bug. 


## 📚 Components 

### <Catalog name> template

This template allows you to <add a description>. 

To integrate this one in your CI/CD pipeline, you have to add:

```yaml
include:
  - component: https://gitlab.com/<your project path>/<name of your template>@<tag>
```

Where `<tag>` is the tag you want to use (cf [releases 🚀](https://gitlab.com/<your project path>/-/releases)).

```yaml
include:
  - component: https://gitlab.com/<your project path>/<name of your template>@<tag>
```

## 🛠️ Inputs

The template contains some variables you can override :

| Input          | Default value       | Description                                                     |
| -------------- | ------------------- | --------------------------------------------------------------- |
| xxx        | xxx         | xxx                   |


